<?php


namespace Firewox\Licensing;


class Error
{


  /**
   * @var string|null
   */
  public $message;


  /**
   * @var int|null
   */
  public $code;


  /**
   * @return string|null
   */
  public function getMessage(): ?string
  {
    return $this->message;
  }


  /**
   * @return int|null
   */
  public function getCode(): ?int
  {
    return $this->code;
  }


}