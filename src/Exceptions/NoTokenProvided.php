<?php

namespace Firewox\Licensing\Exceptions;

class NoTokenProvided extends \Exception
{

    public function __construct(){
        parent::__construct('No token provided.');
    }

}