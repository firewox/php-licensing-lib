<?php

namespace Firewox\Licensing\Exceptions;

class ServerRequestFailed extends \Exception
{

    public function __construct(string $message = ''){
        parent::__construct('Request failed. ' . $message);
    }

}