<?php

namespace Firewox\Licensing\Entities;

use Karriere\JsonDecoder\JsonDecoder;
use Licensing\ContainerEntity;

class LicenseEntity
{

  /**
   * @var string|null
   */
  public $expireson;

  /**
   * @var array|null
   */
  public $features;

  /**
   * @var string|null
   */
  public $reference;

  /**
   * @var array|null
   */
  public $module;

  /**
   * @var array|null
   */
  public $roles;

  /**
   * @var array|null
   */
  public $containers;

  /**
   * @var array|null
   */
  public $groups;


  /**
   * @return string|null
   */
  public function getExpiresOn(): ?string
  {
    return $this->expireson;
  }


  /**
   * @return FeatureEntity[]|null
   */
  public function getFeatures(): ?array
  {

    $decoder = new JsonDecoder();
    $features = $this->features ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, FeatureEntity::class);
    }, $features);

  }


  /**
   * @return string|null
   */
  public function getReference(): ?string
  {
    return $this->reference;
  }


  /**
   * @return ModuleEntity|null
   */
  public function getModule(): ?array
  {

    $decoder = new JsonDecoder();
    $module = $this->module ?: [];
    return $decoder->decodeArray($module, ModuleEntity::class);

  }


  /**
   * @return RoleEntity[]|null
   */
  public function getRoles(): ?array
  {

    $decoder = new JsonDecoder();
    $roles = $this->roles ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, RoleEntity::class);
    }, $roles);

  }


  /**
   * @return ContainerEntity[]|null
   */
  public function getContainers(): ?array
  {

    $decoder = new JsonDecoder();
    $containers = $this->containers ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, ContainerEntity::class);
    }, $containers);

  }


  /**
   * @return SystemGroupEntity[]|null
   */
  public function getGroups(): ?array
  {

    $decoder = new JsonDecoder();
    $groups = $this->groups ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, SystemGroupEntity::class);
    }, $groups);

  }


}