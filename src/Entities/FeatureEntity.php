<?php


namespace Firewox\Licensing\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class FeatureEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $code;

  /**
   * @var array|null
   */
  public $functions;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getCode(): ?string
  {
    return $this->code;
  }


  /**
   * @return array|null
   */
  public function getFunctions(): ?array
  {

    $decoder = new JsonDecoder();
    $functions = $this->functions ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, SystemFunctionEntity::class);
    }, $functions);

  }


}