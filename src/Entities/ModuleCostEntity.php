<?php


namespace Firewox\Licensing\Entities;


class ModuleCostEntity
{

  /**
   * @var float|null
   */
  public $cost;

  /**
   * @var int|null
   */
  public $duration;

  /**
   * @var string|null
   */
  public $durationuom;

  /**
   * @var bool|null
   */
  public $enabled;


  /**
   * @return float|null
   */
  public function getCost(): ?float
  {
    return $this->cost;
  }


  /**
   * @return int|null
   */
  public function getDuration(): ?int
  {
    return $this->duration;
  }


  /**
   * @return string|null
   */
  public function getDurationUOM(): ?string
  {
    return $this->durationuom;
  }


  /**
   * @return bool|null
   */
  public function isEnabled(): ?bool
  {
    return $this->enabled;
  }



}