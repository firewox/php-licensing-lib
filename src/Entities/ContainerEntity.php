<?php


namespace Licensing;


class ContainerEntity
{

  /**
   * @var string|null
   */
  public $code;

  /**
   * @var string|null
   */
  public $name;


  /**
   * @return string|null
   */
  public function getCode(): ?string
  {
    return $this->code;
  }


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


}