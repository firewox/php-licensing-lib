<?php


namespace Firewox\Licensing\Entities;


class SystemGroupEntity
{

  /**
   * @var string|null
   */
  public $name;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


}