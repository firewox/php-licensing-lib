<?php


namespace Firewox\Licensing\Entities;



class SystemFunctionStatusEntity
{

  /**
   * @var bool|null
   */
  public $valid;

  /**
   * @var bool|null
   */
  public $allowed;

  /**
   * @var string|null
   */
  public $code;


  /**
   * @return bool|null
   */
  public function getValid(): ?bool
  {
    return $this->valid;
  }


  /**
   * @return bool|null
   */
  public function isAllowed(): ?bool
  {
    return $this->allowed;
  }


  /**
   * @return string|null
   */
  public function getCode(): ?string
  {
    return $this->code;
  }



}