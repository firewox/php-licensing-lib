<?php


namespace Firewox\Licensing\Entities;


class RoleEntity
{

  /**
   * @var string|null
   */
  public $name;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


}