<?php


namespace Firewox\Licensing\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class ModuleEntity
{

  /**
   * @var string|null
   */
  public $code;

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var array|null
   */
  public $costs;

  /**
   * @var array|null
   */
  public $submodules;

  /**
   * @var bool|null
   */
  public $islicensed;

  /**
   * @return string|null
   */
  public function getCode(): ?string
  {
    return $this->code;
  }


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return array|null
   */
  public function getCosts(): ?array
  {

    $decoder = new JsonDecoder();
    $costs = $this->costs ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, self::class);
    }, $costs);

  }


  /**
   * @return array|null
   */
  public function getSubModules(): ?array
  {

    $decoder = new JsonDecoder();
    $modules = $this->submodules ?: [];

    return array_map(function(array $data) use ($decoder){
      return $decoder->decodeArray($data, self::class);
    }, $modules);

  }


  /**
   * @return bool|null
   */
  public function isLicensed(): ?bool
  {
    return $this->islicensed;
  }


}