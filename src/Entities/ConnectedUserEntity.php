<?php

namespace Firewox\Licensing\Entities;

class ConnectedUserEntity
{

  /**
   * @var string|null
   */
  public $leasorfullname;

  /**
   * @var string
   */
  public $lesseefullname;

  /**
   * @var string|null
   */
  public $leasorusername;

  /**
   * @var string|null
   */
  public $lesseeusername;

  /**
   * @var string|null
   */
  public $token;

  /**
   * @var string|null
   */
  public $clientkey;

  /**
   * @var bool|null
   */
  public $online;

  /**
   * @var string|null
   */
  public $expireson;


  /**
   * @return string|null
   */
  public function getLeasorFullName(): ?string
  {
    return $this->leasorfullname;
  }


  /**
   * @return string
   */
  public function getLesseeFullName(): string
  {
    return $this->lesseefullname;
  }


  /**
   * @return string|null
   */
  public function getLeasorUsername(): ?string
  {
    return $this->leasorusername;
  }


  /**
   * @return string|null
   */
  public function getLesseeUsername(): ?string
  {
    return $this->lesseeusername;
  }


  /**
   * @return string|null
   */
  public function getToken(): ?string
  {
    return $this->token;
  }


  /**
   * @return string|null
   */
  public function getClientKey(): ?string
  {
    return $this->clientkey;
  }


  /**
   * @return bool|null
   */
  public function getOnline(): ?bool
  {
    return $this->online;
  }


  /**
   * @return string|null
   */
  public function getExpiresOn(): ?string
  {
    return $this->expireson;
  }


}