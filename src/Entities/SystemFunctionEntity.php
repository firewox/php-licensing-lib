<?php


namespace Firewox\Licensing\Entities;


class SystemFunctionEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $code;

  /**
   * @var bool|null
   */
  public $activated;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getCode(): ?string
  {
    return $this->code;
  }


  /**
   * @return bool|null
   */
  public function getActivated(): ?bool
  {
    return $this->activated;
  }


}