<?php


namespace Firewox\Licensing;


use Karriere\JsonDecoder\JsonDecoder;

class Response
{

  /**
   * @var int|null
   */
  public $offset;

  /**
   * @var int|null
   */
  public $limit;

  /**
   * @var int|null
   */
  public $count;

  /**
   * @var mixed|null
   */
  public $data;

  /**
   * @var array|null
   */
  public $errors;


  /**
   * @return int|null
   */
  public function getOffset(): ?int
  {
    return $this->offset;
  }


  /**
   * @return int|null
   */
  public function getLimit(): ?int
  {
    return $this->limit;
  }


  /**
   * @return int|null
   */
  public function getCount(): ?int
  {
    return $this->count;
  }


  /**
   * @param string $type
   * @return mixed|null
   */
  public function getData(string $type)
  {

    if(!$this->data) return null;

    $decoder = new JsonDecoder();

    if(isset($this->data[0])) {

      // Map the data to correct type
      return array_map(function(array $data) use ($decoder, $type) {
        return $decoder->decodeArray($data, $type);
      }, $this->data);

    } else {

      // Data is an object
      return $decoder->decodeArray($this->data, $type);

    }

  }


  /**
   * @return array|null
   */
  public function getErrors(): ?array
  {

    $decoder = new JsonDecoder();

    // Map the errors data to correct type
    return array_map(function(array $data) use ($decoder) {
      return $decoder->decodeArray($data, Error::class);
    }, $this->errors);

  }


}