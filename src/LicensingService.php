<?php

namespace Firewox\Licensing;

use Firewox\Licensing\Exceptions\NoResponse;
use Firewox\Licensing\Exceptions\NoTokenProvided;
use Firewox\Licensing\Exceptions\ServerErrorResponse;
use Firewox\Licensing\Exceptions\ServerRequestFailed;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Karriere\JsonDecoder\JsonDecoder;

class LicensingService
{

  private $connect_endpoint;
  private $licenseinfo_endpoint;
  private $modules_endpoint;
  private $config;

  public function __construct(string $connectEndpoint,
                              ?string $licenseInfoEndpoint = null,
                              ?string $modulesEndpoint = null,
                              array $guzzleConfig = [])
  {

    $this->connect_endpoint = $connectEndpoint;
    $this->licenseinfo_endpoint = $licenseInfoEndpoint;
    $this->modules_endpoint = $modulesEndpoint;

    // Get config for guzzle
    $this->config = array_merge(
      [
        'verify' => false       // Ignore SSL errors by default
      ],
      $guzzleConfig
    );

  }


  /**
   * Get HTTP client for guzzle
   * @return Client
   */
  private function getHttpClient(): Client {
    return new Client($this->config);
  }


  public function connect(string $accessToken, string $guid, string $key): ?Response {

    if(!$accessToken) throw new NoTokenProvided();

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();

      $response = $client->post($this->connect_endpoint, [
        'form_params' => [
          'guid' => $guid,
          'key' => $key
        ],
        'headers' => [
          'Authorization' => 'Bearer ' . $accessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getLicenseInformation(string $accessToken, string $connectionToken): ?Response {

    if(!$connectionToken || !$accessToken) throw new NoTokenProvided();

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();
      $url = sprintf('%s/%s', $this->licenseinfo_endpoint, $connectionToken);

      $response = $client->get($url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $accessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getFunctionsAllowed(string $accessToken, string $connectionToken, array $functionCodes): ?Response {

    if(!$connectionToken || !$accessToken) throw new NoTokenProvided();

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();
      $url = sprintf('%s/%s/functions/allowed/%s', $this->licenseinfo_endpoint, $connectionToken, join(',', $functionCodes));

      $response = $client->get($url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $accessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getModules(string $clientGuid): ?Response {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();

      $response = $client->get($this->modules_endpoint . "/{$clientGuid}");

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


}